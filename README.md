# GitLab CI/CD Pipeline for Building and Pushing Docker Images

This repository contains a CI/CD pipeline that automatically builds a Docker image using Podman and pushes it to a specified Docker repository.

## Configuration

### Variables

The pipeline uses the following variables:

- `IMAGE_NAME`: The name of the Docker image. (Default: "your-default-image-name")
- `REPO`: The Docker repository where the image will be pushed. (Default: "your-default-repo")

### Authentication

Before pushing the Docker image, the pipeline logs into Docker using credentials provided through GitLab's secret CI/CD variables:

- `username`: Your Docker username.
- `token`: Your Docker password or token.

Ensure you set these as secret variables in your GitLab project's CI/CD settings to protect them.

### Image Tagging

Each built Docker image is tagged with the unique `$CI_COMMIT_SHORT_SHA` from the GitLab CI/CD pipeline, representing the first 8 characters of the commit or merge request revision. This ensures each build is distinct and can be traced back to a specific commit or merge request in the repository.

## Usage

1. **Set Up Secrets**: Before running the pipeline, make sure to set your Docker `username` and `token` as secret variables in your GitLab project's CI/CD settings.
2. **Trigger Pipeline**: Any push to the repository will trigger the pipeline. Once completed, the image will be available in the specified Docker repository with a tag corresponding to the first 8 characters of the GitLab CI commit, represented as `$CI_COMMIT_SHORT_SHA`.
3. **View Builds**: To see the list of CI/CD job runs, go to the "CI/CD" section of your GitLab project. From there, you can track each job and its corresponding Docker image.

